from waitress import serve
from flaskpage.config import Config
from flaskpage import create_app

app = create_app(Config)

ip = Config.SERVER_NAME.split(":")
serve(app, host=ip[0], port=ip[1])
