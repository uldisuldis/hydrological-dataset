from flask import Flask
from flaskpage.config import Config
from flaskpage.routes import mainBP, errorsBP

def create_app(Config):
    app = Flask(__name__)
    app.config.from_object(Config)
    app.register_blueprint(mainBP)
    app.register_blueprint(errorsBP)

    return app
