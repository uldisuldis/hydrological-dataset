from flaskpage.config import Config
from flaskpage import create_app

app = create_app(Config)

if __name__ == '__main__':
	app.run(debug=True)
