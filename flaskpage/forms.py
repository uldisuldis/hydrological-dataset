from flask_wtf import FlaskForm
from wtforms import SubmitField, BooleanField

class MapForm(FlaskForm):
	map1 = BooleanField('Freshwater')
	map2 = BooleanField('Waterways')
	map3 = BooleanField('Withdrawal')
	map4 = BooleanField('Precipitation')
	map5 = BooleanField('Services')
	map6 = BooleanField('Productivity')
	submit = SubmitField('Post')