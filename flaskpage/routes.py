import folium, geopandas
import pandas as pd
from flask import render_template, url_for, redirect, request, Blueprint, flash
from flaskpage.utils import mergTrim, createMap
from flaskpage.forms import MapForm

mainBP = Blueprint('main', __name__)
errorsBP = Blueprint('errors', __name__)

@mainBP.route("/map", methods=['GET', 'POST'])
def map():
	form = MapForm()
	if request.method == 'POST':
		geoMap = newMap()
		if form.map1.data == True:
			url = "https://www.indexmundi.com/facts/indicators/ER.H2O.INTR.PC/rankings"
			tableWaterRenu = mergTrim(pd.read_html(url)[0])
			i = 0
			while i < len(tableWaterRenu):
				tableWaterRenu.loc[i, "Value"] /= 1000
				i += 1
			createMap(tableWaterRenu, legend="Annual renewable freshwater per capita in 1000 m³", geoMap=geoMap, color="WhGr")
		if form.map2.data == True:
			url = "https://en.wikipedia.org/wiki/List_of_countries_by_waterways_length"
			tableWaterways = mergTrim(pd.read_html(url)[0], value="Waterways (km)")
			createMap(tableWaterways, legend='Waterways in km', geoMap=geoMap, color="WhBl")
		if form.map3.data == True:
			url = "https://en.wikipedia.org/wiki/List_of_countries_by_freshwater_withdrawal"
			tableWaterDraw = mergTrim(pd.read_html(url)[1], value="Per capitawithdrawal(m³/year)")
			createMap(tableWaterDraw, legend="Annual freshwater withdrawal per capita in m³", geoMap=geoMap, color="WhRe")
		if form.map4.data == True:
			url = "https://www.indexmundi.com/facts/indicators/AG.LND.PRCP.MM/rankings"
			tablePrecipitation = mergTrim(pd.read_html(url)[0])
			createMap(tablePrecipitation, legend="Annual average precipitation in mm", geoMap=geoMap, color="BlOr")
		if form.map5.data == True:
			url = "https://www.indexmundi.com/facts/indicators/SH.H2O.BASW.ZS/rankings"
			tableServices = mergTrim(pd.read_html(url)[0])
			createMap(tableServices, legend="% of population with water services", geoMap=geoMap, color="PuWhAq")
		if form.map6.data == True:
			url = "https://www.indexmundi.com/facts/indicators/ER.GDP.FWTL.M3.KD/rankings"
			tableProductivity= mergTrim(pd.read_html(url)[0])
			createMap(tableProductivity, legend="GDP per m³ of total freshwater withdrawal", geoMap=geoMap, color="BlYl")
		geoMap.save("flaskpage/templates/raw_map.html")
		raw = open("flaskpage/templates/raw_map.html", "r")
		full = open("flaskpage/templates/full_map.html", "w")
		for line in raw:
			full.write(line.replace('topright', 'bottomright'))
		raw.close()
		full.close()
		return render_template('map.html', title='Map', form=form)
	if request.method == 'GET':
		geoMap = newMap()
		geoMap.save("flaskpage/templates/full_map.html")
		return render_template('map.html', title='Map', form=form)

def newMap():
	return folium.Map([40,5], zoom_start=3, tiles='cartodbpositron')

@mainBP.route("/full_map")
def full_map():
	return render_template('full_map.html')

@errorsBP.app_errorhandler(404)
def error_404(error):
	return redirect(url_for('main.map'))

@errorsBP.app_errorhandler(403)
def error_403(error):
	return redirect(url_for('main.map'))

@errorsBP.app_errorhandler(500)
def error_500(error):
	return redirect(url_for('main.map'))
