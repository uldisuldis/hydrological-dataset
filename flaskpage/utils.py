import pandas as pd
import geopandas
import folium

def tableHeads(url, column='Country'):
	allTables = pd.read_html(url)
	for table in allTables:
		for row in table[column]:
			print(row)

worldMapOri = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
worldMap = worldMapOri.replace('United States of America', 'United States')

def mergTrim(table, key='Country', value="Value"):
	table = table.replace("Dem. People's Rep. Korea", 'North Korea')
	table = table.replace("Korea, North", 'North Korea')
	table = table.replace('Korea', 'South Korea')
	tableMerg = worldMap.merge(table, how="left", left_on='name', right_on=key)
	tableClean = tableMerg[['geometry', key, value, "pop_est"]].dropna(subset=[value]).reset_index(drop=True)
	return tableClean.rename(columns = {key:'Country', value:'Value', 'pop_est':"Pop"})

def createMap(table, color="PuBu", geoMap=folium.Map([40,5], zoom_start=3, tiles='cartodbpositron'), legend="N/A"):
	folium.Choropleth(
		geo_data=table,
		name='sdsdfsd',
		data=table, 
		columns=["Country", "Value"],
		key_on="feature.properties.Country",
		fill_color=color,
		fill_opacity=0.3,
		line_opacity=0.5,
		legend_name=legend,
		smooth_factor=0
	).add_to(geoMap)
	return geoMap
